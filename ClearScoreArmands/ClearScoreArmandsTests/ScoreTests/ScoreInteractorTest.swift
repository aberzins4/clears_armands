//
//  ScoreInteractorTest.swift
//  ClearScoreArmandsTests
//
//  Created by armands.berzins on 20/06/2019.
//  Copyright © 2019 armandsberzinsdev. All rights reserved.
//

import UIKit
import XCTest
@testable import ClearScoreArmands




class ScoreInteractorTest: XCTestCase, CSScoreInteractionDelegate {
    
    var scoreInteractor: CSScoreInteractor!
    var fakeNetworkManager = FakeNetworkManager()
    private var delegateCalledSuccessfully = false
    private var callExpectation: XCTestExpectation!
    
    struct Result {
        var maxScore: Int
        var score: Int
    }
    private var result: Result!
    
    let fakeValidData = ScoreData (accountIDVStatus: "PASS", creditReportInfo: CreditReportInfo(changeInLongTermDebt: -327, changeInShortTermDebt: 549, changedScore: 0, clientRef: "CS-SED-655426-708782", currentLongTermCreditLimit: nil, currentLongTermCreditUtilisation: nil, currentLongTermDebt: 24682, currentLongTermNonPromotionalDebt: 24682, currentShortTermCreditLimit: 30600, currentShortTermCreditUtilisation: 44, currentShortTermDebt: 13758, currentShortTermNonPromotionalDebt: 13758, daysUntilNextReport: 9, equifaxScoreBand: 4, equifaxScoreBandDescription: "Excellent", hasEverBeenDelinquent: true, hasEverDefaulted: false, maxScoreValue: 1000, minScoreValue: 0, monthsSinceLastDefaulted: -1, monthsSinceLastDelinquent: 1, numNegativeScoreFactors: 0, numPositiveScoreFactors: 9, percentageCreditUsed: 44, percentageCreditUsedDirectionFlag: 1, score: 300, scoreBand: 4, status: "MATCH"), dashboardStatus: "PASS", personaType: "INEXPERIENCED", coachingSummary: CoachingSummary(activeTodo: false, activeChat: true, numberOfTodoItems: 0, numberOfCompletedTodoItems: 0, selected: true), augmentedCreditScore: nil)

    public class FakeNetworkManager: CSNetworkEntity {
            public var successResponse: Decodable?
            public var errorResponse: String?
            open override func get<T>(urlString: String,
                                      headers: [String : String],
                                      successHandler: @escaping (T) throws -> Void,
                                      errorHandler: @escaping ErrorHandler) where T : Decodable {
                switch successResponse {
                case .some(let response):
                    if let correctResponse = response as? T {
                        do {
                            try successHandler(correctResponse)
                        } catch {
                            errorHandler(.invalidData)
                        }
                    } else {
                        errorHandler(.invalidData)
                    }
                default:
                    errorHandler(.otherError)
                }
            }
    }

    override func setUp() {
    /* Reset delegate and data manager */
        super.setUp()
        callExpectation = expectation(description: "DelegateCalled")
        result = Result(maxScore: 0, score: 0)
        delegateCalledSuccessfully = false
        fakeNetworkManager = FakeNetworkManager()
        scoreInteractor = CSScoreInteractor(networkLayer: fakeNetworkManager)
        scoreInteractor.scoreInteractionDelegate = self
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /* CSSCoreInteractorDelegate method */
    func getUserScoresToDisplay(validMaxScore: Int, validScore: Int) {
        delegateCalledSuccessfully = true
        result.maxScore = validMaxScore
        result.score = validScore
        callExpectation.fulfill()
    }
    
    func errorDetected(error: ScoreError) {
        /* ! Add tests for error detected ! */
        print(error)
    }
    
    /* Tests */
    func test_getScoreData_callsGetUserScoresToDisplay_onSuccess() {
        fakeNetworkManager.successResponse = fakeValidData
         scoreInteractor.getScoreData()
        waitForExpectations(timeout: 2)
        XCTAssertTrue(delegateCalledSuccessfully, "should call getUserScoreToDisplay if score data was valid")
    }
    
    func test_getScoreData_callsGetUserScoresToDisplay_onFail() {
        callExpectation.isInverted = true
        fakeNetworkManager.successResponse = nil
        scoreInteractor.getScoreData()
        waitForExpectations(timeout: 2)
        XCTAssertFalse(delegateCalledSuccessfully, "should not call getUserScoreToDisplay if score data was valid")
    }
    
    func test_getScoreData_callsGetUserScoresToDisplay_correctValuesProcessed() {
        fakeNetworkManager.successResponse = fakeValidData
        scoreInteractor.getScoreData()
        waitForExpectations(timeout: 2)
        XCTAssertEqual(result.maxScore, fakeValidData.creditReportInfo.maxScoreValue)
        XCTAssertEqual(result.score, fakeValidData.creditReportInfo.score)
    }
    
    func test_getScoreData_callsGetUserScoresToDisplay_incorrectValuesProcessed() {
        callExpectation.isInverted = true
        fakeNetworkManager.successResponse = nil
        scoreInteractor.getScoreData()
        waitForExpectations(timeout: 2)
        XCTAssertNotEqual(result.maxScore, fakeValidData.creditReportInfo.maxScoreValue)
        XCTAssertNotEqual(result.score, fakeValidData.creditReportInfo.score)
    }
}
