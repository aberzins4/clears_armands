//
//  BalanceInteractor.swift
//  ClearScoreArmands
//
//  Created by Armands Berzins on 10/06/2019.
//  Copyright © 2019 armandsberzinsdev. All rights reserved.
//

import Foundation
import CoreData
import UIKit

protocol CSScoreInteractionDelegate: AnyObject {
    func getUserScoresToDisplay(validMaxScore: Int, validScore: Int)
    func errorDetected(error: ScoreError)
}

class CSScoreInteractor {
    var scoreInteractionDelegate: CSScoreInteractionDelegate?
    let networkLayer: CSNetworkEntity
    
    init(networkLayer: CSNetworkEntity = CSNetworkEntity()) {
        self.networkLayer = networkLayer
    }
    
    func getScoreData() -> Void {
        let successHandler: (ScoreData) throws -> Void = { (score) in
                if let validMaxScore = score.creditReportInfo.maxScoreValue, let validScore = score.creditReportInfo.score {
                    self.checkIfValuesAreRealisticAndUpdatePresenter(maxScore: validMaxScore, score: validScore)
                } else {
                    self.scoreInteractionDelegate?.errorDetected(error: .invalidData)
                }
        }
        let errorHandler: (ScoreError) -> Void = { (networkManagerError) in
            self.scoreInteractionDelegate?.errorDetected(error: networkManagerError)
        }
        if Reachability.isConnectedToNetwork() {
            networkLayer.get(urlString: prepareGetScoreUrl(forUser: "mockcredit"), successHandler: successHandler, errorHandler: errorHandler)
        } else {
            self.scoreInteractionDelegate?.errorDetected(error: .noNetwork)
            DispatchQueue.main.async { [weak self] in
                if let validMaxScore = self?.loadScoreToCoreData().maxScore, let validScore = self?.loadScoreToCoreData().score {
                    self?.scoreInteractionDelegate?.getUserScoresToDisplay(validMaxScore: validMaxScore, validScore: validScore)
                }
            }
        }
    }
    
    func prepareGetScoreUrl(forUser user: String) -> String {
        return CSNetworkEntity.ClearScoreEndpoints.prodUrl.rawValue + "/\(user)/values"
    }
    
    func checkIfValuesAreRealisticAndUpdatePresenter(maxScore: Int, score: Int) -> Void {
        if (maxScore >= score) {
            DispatchQueue.main.async { [weak self] in
                self?.saveScoreToCoreData(maxScore: maxScore, score: score)
                self?.scoreInteractionDelegate?.getUserScoresToDisplay(validMaxScore: maxScore, validScore: score)
            }
        } else {
            if (score < maxScore) && (score > -1 ) {
                self.scoreInteractionDelegate?.errorDetected(error: .invalidScore)
            } else {
                self.scoreInteractionDelegate?.errorDetected(error: .invalidMaxScore)
            }
        }
    }
    
    func saveScoreToCoreData(maxScore: Int, score: Int) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let now = Date(timeIntervalSinceNow: 0)
        let entity = NSEntityDescription.entity(forEntityName: "LastScore", in: context)
        let newLastScore = NSManagedObject(entity: entity!, insertInto: context)
        newLastScore.setValue(maxScore, forKey: "maxScore")
        newLastScore.setValue(score, forKey: "score")
        newLastScore.setValue(now, forKey: "lastUpdate")
        do {
            try context.save()
        } catch {
            print("Failed saving to CoreData")
        }
    }
    
    func loadScoreToCoreData() -> (score: Int?, maxScore: Int?, lastDate: Date) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LastScore")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                return (score: (data.value(forKey: "score") as! Int), maxScore: (data.value(forKey: "maxScore") as! Int), lastDate:(data.value(forKey: "lastUpdate") as! Date))
            }
        } catch {
            print("Failed load from CoreData")
            return (score: 0, maxScore: 0, lastDate:Date())
        }
        return (score: 0, maxScore: 0, lastDate:Date())
    }
}
