//
//  CSScoreViewPresenter.swift
//  ClearScoreArmands
//
//  Created by armands.berzins on 12/06/2019.
//  Copyright © 2019 armandsberzinsdev. All rights reserved.
//

import UIKit

protocol CSScoreViewPresenterDelegate: AnyObject {
    func updateScoreResult(maxScore:Int, currentScore:Int)
    func setAnimatedRingValues(animationRingProportion: CGFloat, ringColor: UIColor, animateFrom: CGFloat, animationDuration: TimeInterval)
    func removePreviousAnimatedCircleView()
    func presentErrorView(error: ScoreError)
}

class CSScoreViewPresenter {
    var viewPresenterDelegate: CSScoreViewPresenterDelegate?
    let interactor = CSScoreInteractor()
    
    init() {
        interactor.scoreInteractionDelegate = self
        interactor.getScoreData()
    }
    
    func updateViewValuesWithScoreParamters(maxScore: Int, currentScore: Int) -> Void {
            self.viewPresenterDelegate?.updateScoreResult(maxScore: maxScore, currentScore: currentScore)
            let animationRingProportion = CGFloat(currentScore)/CGFloat(maxScore)
            self.viewPresenterDelegate?.setAnimatedRingValues(animationRingProportion: animationRingProportion, ringColor: setupRingColor(hueValue: calculateHueValue(proportion: animationRingProportion)), animateFrom: setAnimationStart(), animationDuration: setRingAnimationDuration())
    }
    
    func calculateHueValue(proportion: CGFloat) -> CGFloat {
        return proportion/4
    }
    
    func setupRingColor(hueValue: CGFloat) -> UIColor {
        return UIColor(hue: hueValue, saturation: 1.0, brightness: 0.88, alpha: 1.0)
    }
    
    func setAnimationStart() -> CGFloat {
        return 0
    }
    
    func setRingAnimationDuration() -> TimeInterval {
        return 2
    }
    
    func navigateToShowAuthor(from view: UIViewController) {
        let router = ScoreRouter()
        router.pushToFruitDetail(from: view)
    }
    
    func refreshUserData() -> Void {
        self.viewPresenterDelegate?.removePreviousAnimatedCircleView()
        interactor.getScoreData()
    }
}

extension CSScoreViewPresenter: CSScoreInteractionDelegate {
    func errorDetected(error: ScoreError) {
        DispatchQueue.main.async { [weak self] in
            self?.viewPresenterDelegate?.presentErrorView(error: error)
        }
    }
    func getUserScoresToDisplay(validMaxScore: Int, validScore: Int) {
        self.updateViewValuesWithScoreParamters(maxScore: validMaxScore, currentScore: validScore)
    }
}
