//
//  ViewController.swift
//  ClearScoreArmands
//
//  Created by armands.berzins on 02/06/2019.
//  Copyright © 2019 armandsberzinsdev. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var creditScoreLabel: UILabel!
    @IBOutlet weak var numberScoreLabel: UILabel!
    @IBOutlet weak var totalScoreLabel: UILabel!
    
    @IBOutlet weak var circleTopMargin: NSLayoutConstraint!
    @IBOutlet weak var yourCreditScoreLeft: NSLayoutConstraint!
    @IBOutlet weak var yourCredtiScoreRight: NSLayoutConstraint!
    @IBOutlet weak var yourCrediScoreHieght: NSLayoutConstraint!
    @IBOutlet weak var scoreLabelHeight: NSLayoutConstraint!
    
    
    var semiCircleLayer   = CAShapeLayer()
    let presenter = CSScoreViewPresenter()
    weak var delegate: CSScoreViewPresenterDelegate?
    var loadingView: LoadingView!
    
    enum OutsideFrameBorderWidth: CGFloat {
        case normal = 1.5
        case invisible = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.viewPresenterDelegate = self
        setupUserInterface(title: "Dashboard")
    }
    
    func setupUserInterface(title: String) -> Void {
        showLoading()
        self.title = title
        creditScoreLabel.text = "Your credit score is"
        numberScoreLabel.text = ""
        totalScoreLabel.text = ""
        
        /* Round container -large circle- view and add frame */
        self.containerView.layer.cornerRadius = self.containerView.layer.frame.size.width/2
        self.containerView.clipsToBounds = true
        self.containerView.layer.borderColor = UIColor.black.cgColor
        self.containerView.layer.borderWidth = OutsideFrameBorderWidth.normal.rawValue
        
        /*Adjust circle inside constraints */
        self.circleTopMargin.constant = self.containerView.frame.height / 4
        yourCreditScoreLeft.constant = self.containerView.frame.width / 6
        yourCredtiScoreRight.constant = yourCreditScoreLeft.constant
        yourCrediScoreHieght.constant = self.containerView.frame.height / 16
    }
    
    func showLoading(){
        loadingView = LoadingView(frame: self.view.frame)
        self.view.addSubview(loadingView)
    }
    
    func hideLoading(){
        loadingView.removeFromSuperview()
    }
    
    @IBAction func aboutDeveloperButtonAction(_ sender: UIButton) {
        presenter.navigateToShowAuthor(from: self)
    }
    
    @IBAction func refreshButtonAction(_ sender: UIBarButtonItem) {
        showLoading()
        presenter.refreshUserData()
        self.containerView.layer.borderWidth = OutsideFrameBorderWidth.normal.rawValue
    }
}

extension ScoreViewController: CSScoreViewPresenterDelegate {
    func updateScoreResult(maxScore: Int, currentScore: Int) {
        hideLoading()
        numberScoreLabel.text = String(currentScore)
        totalScoreLabel.text = "out of \(maxScore)"
    }
    
    func setAnimatedRingValues(animationRingProportion: CGFloat, ringColor: UIColor, animateFrom: CGFloat, animationDuration: TimeInterval) {
        numberScoreLabel.textColor = ringColor
        let aniRingView = AnimatedRingView(frame: CGRect(x: 5, y: 5, width: containerView.frame.width-20, height: containerView.frame.height-20)) //might differ depend on view
        aniRingView.proportion = animationRingProportion
        containerView.addSubview(aniRingView)
        aniRingView.animateRing(From: animateFrom, To: animationRingProportion, Duration: animationDuration, ringColor: ringColor)
    }
    
    func removePreviousAnimatedCircleView() {
        for v in containerView.subviews{
            if v is AnimatedRingView{
                v.removeFromSuperview()
            }
        }
    }
    
    func presentErrorView(error: ScoreError) {
        hideLoading()
        error.alert(with: self)
        if error != .noNetwork {
            creditScoreLabel.text = "Error detected"
            numberScoreLabel.text = "👩‍💻👨‍🔧🔜"
            totalScoreLabel.text = "Please try again later"
            self.containerView.layer.borderWidth = OutsideFrameBorderWidth.invisible.rawValue
        }
    }
}
