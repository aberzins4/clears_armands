//
//  ScoreViewRouter.swift
//  ClearScoreArmands
//
//  Created by armands.berzins on 18/06/2019.
//  Copyright © 2019 armandsberzinsdev. All rights reserved.
//

import UIKit

struct ScoreRouter {
    func pushToFruitDetail(from view: UIViewController) {
        let authorViewController = view.storyboard?.instantiateViewController(withIdentifier: "AuthorViewController") as! AuthorViewController
        view.navigationController?.pushViewController(authorViewController, animated: true)
    }
}

