//
//  ViewController.swift
//  ClearScoreArmands
//
//  Created by armands.berzins on 02/06/2019.
//  Copyright © 2019 armandsberzinsdev. All rights reserved.
//

import UIKit

class AuthorViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Author"
        self.navigationController?.navigationBar.tintColor = .white
    }
}
