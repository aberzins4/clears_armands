//
//  CSNetworkEntity.swift
//  ClearScoreArmands
//
//  Created by armands.berzins on 02/06/2019.
//  Copyright © 2019 armandsberzinsdev. All rights reserved.
//

import Foundation
typealias NetworkCompletionHandler = (Data?, URLResponse?, Error?) -> Void
typealias ErrorHandler = (ScoreError) -> Void


class CSNetworkEntity {
    enum ClearScoreEndpoints: String {
        case prodUrl = "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/prod"
    }
    
    func get<T: Decodable>(urlString: String,
                           headers: [String: String] = [:],
                           successHandler: @escaping (T) throws -> Void,
                           errorHandler: @escaping ErrorHandler) {
        let completionHandler: NetworkCompletionHandler = { (data, urlResponse, error) in
            if let error = error {
                print(error.localizedDescription)
                errorHandler(.serverError)
                return
            }
            
            if self.isSuccessCode(urlResponse) {
                guard let data = data else {
                    print("Unable to parse the response in given type \(T.self)")
                    return errorHandler(.invalidData)
                }
                if let responseObject = try? JSONDecoder().decode(T.self, from: data) {
                    do {
                        try successHandler(responseObject)
                    } catch {
                        errorHandler(.invalidData)
                    }
                    return
                }
            }
            errorHandler(.invalidData)
        }
        
        guard let url = URL(string: urlString) else {
            return errorHandler(.wrongUrl)
        }
        
        var request = URLRequest(url: url)
        request.allHTTPHeaderFields = headers
        URLSession.shared.dataTask(with: request, completionHandler: completionHandler).resume()
    }
    private func isSuccessCode(_ statusCode: Int) -> Bool {
        return statusCode >= 200 && statusCode < 300
    }
    private func isSuccessCode(_ response: URLResponse?) -> Bool {
        guard let urlResponse = response as? HTTPURLResponse else {
            return false
        }
        return isSuccessCode(urlResponse.statusCode)
    }
}
