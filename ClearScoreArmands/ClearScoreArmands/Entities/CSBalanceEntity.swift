//
//  CSBalanceEntity.swift
//  ClearScoreArmands
//
//  Created by armands.berzins on 10/06/2019.
//  Copyright © 2019 armandsberzinsdev. All rights reserved.
//

import Foundation

struct CreditReportInfo: Codable {
    let changeInLongTermDebt: Int
    let changeInShortTermDebt: Int
    let changedScore: Int
    let clientRef: String
    let currentLongTermCreditLimit: String? //?
    let currentLongTermCreditUtilisation: String? //?
    let currentLongTermDebt: Int
    let currentLongTermNonPromotionalDebt: Int
    let currentShortTermCreditLimit: Int
    let currentShortTermCreditUtilisation: Int
    let currentShortTermDebt: Int
    let currentShortTermNonPromotionalDebt: Int
    let daysUntilNextReport: Int
    let equifaxScoreBand: Int
    let equifaxScoreBandDescription: String //different type
    let hasEverBeenDelinquent: Bool
    let hasEverDefaulted: Bool
    let maxScoreValue: Int?
    let minScoreValue: Int?
    let monthsSinceLastDefaulted: Int
    let monthsSinceLastDelinquent: Int
    let numNegativeScoreFactors: Int
    let numPositiveScoreFactors: Int
    let percentageCreditUsed: Int
    let percentageCreditUsedDirectionFlag: Int
    let score: Int?
    let scoreBand: Int
    let status: String //different type
}

struct CoachingSummary: Codable {
    let activeTodo: Bool
    let activeChat: Bool
    let numberOfTodoItems: Int
    let numberOfCompletedTodoItems: Int
    let selected: Bool
}

struct ScoreData: Codable {
    let accountIDVStatus: String
    let creditReportInfo: CreditReportInfo
    let dashboardStatus: String
    let personaType: String
    let coachingSummary: CoachingSummary
    let augmentedCreditScore: String?

}
