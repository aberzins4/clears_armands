This app is a test task for ClearScore job application.
One of requirements was to make this app in VIPER architecture that I had no experience before.
Project was not made in TDD style - make tests first and code after that - because of this requirement of VIPER architecture. So I decided to go more classic approach and make app working first. Sure, it would be better to redo app again in TDD when I got first success of VIPER but considering limited time I had so many other ideas I need to also add to app not to re make app again and added tests later.

Installation:
Just open the project file with Xcode 10.2 or newer

Setup:
No other steps need - project was intended to be easly compilable

Features:
App shows credit data from back-end in beautiful dougnat stiled view


NB:
Since this is demo app, it was made when I got time after business hours. I still see many places for improvements and even such small app can be polished more and more and who knows when I would reach to the point when I have no idea how to make this app better. So I simply got to stop at some point but it should be sufficent to get idea of me. 
I decided to add a list of planned improvements maybe it would help evaluating my code and also you might ask me to add some point and recommit some point if you see it as neededed for better evaluation.

Planned Improvements:
* Unit tests for Interactor got outdated since they dont cover functions added in last 3 days;
* CoreData part in Interactor is not coverd with tests;
* Data storing for offline mode should be made as Storage Manager since that might be reused by other Interactors;
* CoreData part in Interactor is not coverd with tests;
* CoreData part is way to hardcoded strings it should be casted from class;
* CoreData part in Interactor is not coverd with tests;
* Recheck code for strong referene cycles and avoid them;
* Recheck code for force unwrapps and safe avoid them;
* Optimize DispatchQueue.main.async amount;
* Network Entity (I named it like that before I understood that Entity is only for modules) is actally network manager or network layer and the whole nameing confused across code. Should refactor to same naming;
* Add Unit tests for managers;
* Add possibility for user to see last updated date;
* Refresh would be more modern if it would be swipe to refresh and not old button;
* User should be able to see message that his network is too slow;
* Add Unit tests for presenters;
* Add Unit tests for router;
* Add Unit tests for entities;
* Mockcredit user is hardcoded. While yes we have only one user, user changing and user validating should be added;
* Refactor Animated Ring View - make more like extension;
* Add UIApperiance;
* Add UITests;
* Add Unit tests for entities;
* Low priority: Update About Developer page and move this button in some settings page or something;
* Low priority: This app would make sense to have Apple Watch app or widget or siri support or anyhtinhg that users 'fast chceck';


